let gulp = require('gulp');
let html = require('gulp-htmlmin');
let sass = require('gulp-sass');
let notify = require('gulp-notify');
let browserSync = require('browser-sync').create();

// Tarefa de minificar o HTML
gulp.task('html', function() {
  return gulp.src('./src/index.html')
    .pipe(html({collapseWhitespace: true}))
    .on('error', notify.onError('Error: <%= error.message %>'))
    .pipe(gulp.dest('./dist/'))
    .pipe(browserSync.stream())
})

// Tarefa de passar o SASS para CSS e minificar o CSS
gulp.task('sass', function() {
  return gulp.src('./src/scss/style.scss')
    .pipe(sass({outputStyle: 'compressed'}))
    .on('error', notify.onError('Error: <%= error.message %>'))
    .pipe(gulp.dest('./src/css/'))
    .pipe(gulp.dest('./dist/css/'))
    .pipe(browserSync.stream())
})  

// Tarefa do browserSync serve para gerar um servidor
// junto com ele assistimos as mudanças dos arquivos 
gulp.task('bs', ['html', 'sass'], function() {
  browserSync.init({
    server: {
      baseDir: './src/'
    }
  })
  gulp.watch('./src/index.html', ['html']);
  gulp.watch('./src/scss/style.scss', ['sass']);
})

gulp.task('default', ['bs']); 

// gulp.task('default', ['html', 'sass'], function() {
//   gulp.watch('./src/index.html', ['html']);
//   gulp.watch('./src/scss/style.scss', ['sass']);
// }); 